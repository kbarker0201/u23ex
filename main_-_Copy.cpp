#include <SFML/Graphics.hpp>
#include<iostream>
using namespace std;

const int SCREEN_WIDTH = 800; 
const int SCREEN_HEIGHT = 600;

sf::RenderWindow window(sf::VideoMode(
    SCREEN_WIDTH, SCREEN_HEIGHT), "Example game");

window.setFramerateLimit(60);

map<string, sf::Texture> textures;
textures["bunny"].loadFromFile("bunny.png");
textures["diamond"].loadFromFile("diamond.png");
textures["grass"].loadFromFile("grass.png");

sf::Font font;
font.loadFromFile("PressStart2P.ttf");

sf::Sprite player;
player.setTexture(textures["bunny"]);
player.setPosition(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2);
float playerSpeed = 5;
int playerScore = 0;

sf::Sprite item;
item.setTexture(textures["diamond"]);
item.setPosition(GetRandomPosition(SCREEN_WIDTH, SCREEN_HEIGHT));

sf::Text scoreText;
scoreText.setFont(font);
scoreText.setCharacterSize(30);
scoreText.setFillColor(sf::Color::White);
scoreText.setString("Score: " + to_string(playerScore));

vector<sf::Sprite> groundTiles;
sf::Sprite ground;
ground.setTexture(textures["grass"]);

for (int y = 0; y < SCREEN_HEIGHT; Y += 32)
{
    for (int x = 0; x < SCREEN_WIDTH; x += 32)
    {
        ground.setPosition(x, y);
        groundTiles.push_back(ground);
    }
}



sf::Vector2f GetRandomPosition(int screenWidth, int screenHeight)
{
    sf::Vector2f pos;
    pos.x = rand() % screenWidth - 64;
    pos.y = rand() % screenHeight - 64;
    return pos;
}

float GetDistance(sf::Vector2f obj1, sf::Vector2f obj2)
{
    return sqrt(pow(obj1.x - obj2.x, 2) + pow(obj1.y - obj2.y, 2));
}

int main()
{
    sf::RenderWindow window(sf::VideoMode(200, 200), "SFML works!");
    sf::CircleShape shape(100.f);
    shape.setFillColor(sf::Color::Green);

    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }

        sf::Vector2f playerPos = player.getPosition();
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
        {
            playerPos.x -= playerSpeed;
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
        {
            playerPos.x += playerSpeed;
        }

        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
        {
            playerPos.y -= playerSpeed;
        }
        else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
        {
            playerPos.y += playerSpeed;
        }
        player.setPosition(playerPos);

        if (GetDistance(player.getPosition(), item.getPosition()) < 32)
        {
            //Count this as a collect
            item.setPosition(GetRandomPosition(SCREEN_WIDTH, SCREEN_HEIGHT));
            playerScore++;
            scoreText.setString("Score: " + to_string(playerScore));
        }

        window.clear();

        // Draw all the backgrounds
        for (auto& tile : groundTiles)
        {
            window.draw(tile);
        }

        // Draw item
        window.draw(item);
        //Draw player
        window.draw(player);
        // Draw text
        window.draw(scoreText);

        window.display();
    }
    return 0;
}